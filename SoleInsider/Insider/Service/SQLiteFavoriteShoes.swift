//
//  SQLiteFavoriteShoes.swift
//  Insider
//
//  Created by admin on 27/07/2021.
//



import SQLite
import Foundation
import UIKit

extension FileManager {
    func copyfileToUserDocumentDirectory(forResource name: String,
                                         ofType ext: String) throws -> String
    {
        if let bundlePath = Bundle.main.path(forResource: name, ofType: ext),
            let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                .userDomainMask,
                                true).first {
            let fileName = "\(name).\(ext)"
            let fullDestPath = URL(fileURLWithPath: destPath)
                                   .appendingPathComponent(fileName)
            let fullDestPathString = fullDestPath.path

            if !self.fileExists(atPath: fullDestPathString) {
                try self.copyItem(atPath: bundlePath, toPath: fullDestPathString)
                
            }
            return fullDestPathString
        }
        return ""
    }
}
class SQLiteFavoriteShoes: NSObject {
    static let shared:SQLiteFavoriteShoes = SQLiteFavoriteShoes()
    public var connection:Connection?
    public let databaceFileName = "DataShoes.db"
    //hứng data
    var dataShoesFav:[releaseDatesUnformattedModel] = [releaseDatesUnformattedModel]()
    var dataModelDic = [releaseDatesUnformattedModel]()

    var saveWordDatadic = [releaseDatesUnformattedModel]()

    var addListFovouriteShoes = [releaseDatesUnformattedModel]()
    var getDataFavoriteShoes = [releaseDatesUnformattedModel]()
    // khoi tao serviece
    //    var dataAll = [String:[AddDicModel]]()
    //ket noi toi database
    func initProApp() {
        do {
            let fileManager = FileManager.default
            let dbPath = try fileManager.copyfileToUserDocumentDirectory(forResource: "DataShoes", ofType: "db")
            print(dbPath)
            connection = try Connection ("\(dbPath)")
        }catch{
            connection = nil
            let nserror = error as NSError
            print("Cannot connect to Databace. Error is: \(nserror), \(nserror.userInfo)")
        }
    }
    
    func getDataDic(closure: @escaping (_ response: [releaseDatesUnformattedModel]?, _ error: Error?) -> Void) {
        let users = Table("DataShoes")
        let stockx_thumbnail_url = Expression<String?>("stockx_thumbnail_url")
        let name = Expression<String?>("name")
        let release_date = Expression<String?>("release_date")
        let price = Expression<String?>("prime")
        let fav = Expression<Int?>("fav")
        let idpro = Expression<String?>("id")
        let direction = Expression<Int>("direction")
        
        dataModelDic.removeAll()
        if let DatabaseRoot = connection {
            do{
                for user in try DatabaseRoot.prepare(users) {
                    let valueModelDic = releaseDatesUnformattedModel(
                        name: user[name] ?? "",
                        fav: user[fav] ?? 0,
                        idpro: user[idpro] ?? "",
                        stockx_thumbnail_url: user[stockx_thumbnail_url] ?? "",
                        price: user[price] ?? "",
                        release_date: user[release_date] ?? "")
                    dataModelDic.append(valueModelDic)
                }
            }catch{
                print("Error!")
                return
            }
        }
        closure(dataModelDic, nil)
    }
    
    
    //addFavourite
    func addFavoriteDatabase(itemSend:AddDicModel, closure: @escaping (_ response: AddDicModel?, _ error: Error?) -> Void) {
        let users = Table("DataShoes")
        let fav = Expression<Int>("fav")
        let idpro = Expression<Int64?>("id")
        let name = Expression<String?>("name")
        let stockx_thumbnail_url = Expression<String?>("stockx_thumbnail_url")
        let release_date = Expression<String?>("release_date")
        let price = Expression<String?>("prime")

        if let DatabaceRoot = connection {
            do {
                let insert = users.insert(idpro <- Int64(itemSend.idpro), name <- itemSend.name, fav <- itemSend.fav,stockx_thumbnail_url <- itemSend.stockx_thumbnail_url,release_date <- itemSend.release_date,price <- itemSend.price)
                try DatabaceRoot.run(insert)
            } catch{
                print(error)
                closure(nil, nil)
                return
            }
        }

        closure(itemSend, nil)
    }
    
}
