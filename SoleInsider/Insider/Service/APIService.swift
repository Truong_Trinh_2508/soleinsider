//
//  APIService.swift
//  Insider
//
//  Created by admin on 06/07/2021.
//

import UIKit

typealias ApiCompletion = (_ data: Any?, _ error: Error?) -> ()

typealias ApiParam = [String: Any]

enum ApiMethod: String {
    case GET = "GET"
    case POST = "POST"
}
extension String {
    func addingPercentEncodingForURLQueryValue() -> String? {
        let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~")
        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
    }
}

extension Dictionary {
    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { (key, value) -> String in
            let percentEscapedKey = (key as! String).addingPercentEncodingForURLQueryValue()!
            if value is String {
                let percentEscapedValue = (value as! String).addingPercentEncodingForURLQueryValue()!
                return "\(percentEscapedKey)=\(percentEscapedValue)"
            }
            else {
                return "\(percentEscapedKey)=\(value)"
            }
        }
        return parameterArray.joined(separator: "&")
    }
}
class APIService:NSObject {
    static let shared: APIService = APIService()

    func requestSON(_ url: String,
                    param: ApiParam?,
                    method: ApiMethod,
                    loading: Bool,
                    completion: @escaping ApiCompletion)
    {
        var request:URLRequest!
        
        // set method & param
        if method == .GET {
            if let paramString = param?.stringFromHttpParameters() {
                request = URLRequest(url: URL(string:"\(url)?\(paramString)")!)
            }
            else {
                request = URLRequest(url: URL(string:url)!)
            }
        }
        else if method == .POST {
            request = URLRequest(url: URL(string:url)!)
            
            // content-type
            let headers: Dictionary = ["Content-Type": "application/json"]
            request.allHTTPHeaderFields = headers
            
            do {
                if let p = param {
                    request.httpBody = try JSONSerialization.data(withJSONObject: p, options: .prettyPrinted)
                }
            } catch { }
        }
        
        request.timeoutInterval = 30
        request.httpMethod = method.rawValue
        
        //
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            DispatchQueue.main.async {
                
                // check for fundamental networking error
                guard let data = data, error == nil else {
                    completion(nil, error)
                    return
                }
                
                // check for http errors
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200, let res = response {
                }
                
                if let resJson = self.convertToJson(data) {
                    completion(resJson, nil)
                }
                else if let resString = String(data: data, encoding: .utf8) {
                    completion(resString, error)
                }
                else {
                    completion(nil, error)
                }
            }
        }
        task.resume()
    }
    
    private func convertToJson(_ byData: Data) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: byData, options: [])
        } catch {
            //            self.debug("convert to json error: \(error)")
        }
        return nil
    }
    
    func getComments_6912(closure: @escaping (_ response: [getCommentsModel_6912]?, _ error: Error?) -> Void) {
        requestSON("http://soleinsider.com/mobileapi/getComments?product_id=6912", param: nil, method: .GET, loading: true) { (data, error) in
            if let d = data as? [String: Any] {
                var listSetingCommentID:[String] = [String]()
                             var listComicReturn:[getCommentsModel_6912] = [getCommentsModel_6912]()
                             if let commentIds = d["commentIds"] as? [String] {
                                 for item1 in commentIds{
                                     listSetingCommentID.append(item1)
                                 }
                             }
                if let comments = d["comments"] as? [String:Any] {
                                   for item in listSetingCommentID{
                                       if let itemJson = comments[item] as? [String:Any]{
                                           let itemCommentID :getCommentsModel_6912 = getCommentsModel_6912()
                                           itemCommentID.initLoad(itemJson)
                                           listComicReturn.append(itemCommentID)
                                       }
                                   }
                               }
                closure(listComicReturn, nil)
            }
            else {
                closure(nil,nil)
            }
        }
    }
    func getComments_7209(closure: @escaping (_ response: [getCommentsModel_7209]?, _ error: Error?) -> Void) {
        requestSON("http://soleinsider.com/mobileapi/getComments?product_id=7209", param: nil, method: .GET, loading: true) { (data, error) in
            if let d = data as? [String: Any] {
                var listSetingCommentID:[String] = [String]()
                var listComicReturn:[getCommentsModel_7209] = [getCommentsModel_7209]()
                if let commentIds = d["commentIds"] as? [String] {
                for item1 in commentIds{
                                     listSetingCommentID.append(item1)
                                 }
                             }
                if let comments = d["comments"] as? [String:Any] {
                                   for item in listSetingCommentID{
                                       if let itemJson = comments[item] as? [String:Any]{
                                           let itemCommentID :getCommentsModel_7209 = getCommentsModel_7209()
                                           itemCommentID.initLoad(itemJson)
                                           listComicReturn.append(itemCommentID)
                                       }
                                   }
                               }
                closure(listComicReturn, nil)
            }
            else {
                closure(nil,nil)
            }
        }
    }
    
    func getReleaseDatesUnformatted(closure: @escaping (_ response: [releaseDatesUnformattedModel]?, _ error: Error?) -> Void) {
        requestSON("http://soleinsider.com/mobileapi/releaseDatesUnformatted", param: nil, method: .GET, loading: true) { (data, error) in
            if let data = data as? [[String: Any]] {
                var listReturnData:[releaseDatesUnformattedModel] = [releaseDatesUnformattedModel]()
                for item in data{
                    var itemData:releaseDatesUnformattedModel = releaseDatesUnformattedModel(name: "", fav: 0, idpro: "",stockx_thumbnail_url:"",price:"",release_date:"")
                    itemData = itemData.initload(item)
                    listReturnData.append(itemData)
                }
                closure(listReturnData,nil)
            }
            else {
                closure(nil,nil)
            }
        }
    }
    func getClothingReleaseDatesUnformatted(closure: @escaping (_ response: [clothingReleaseDatesUnformattedModel]?, _ error: Error?) -> Void) {
        requestSON("http://soleinsider.com/mobileapi/clothingReleaseDatesUnformatted", param: nil, method: .GET, loading: true) { (data, error) in
            if let data = data as? [[String: Any]] {
                var listReturnData:[clothingReleaseDatesUnformattedModel] = [clothingReleaseDatesUnformattedModel]()
                for item in data{
                    var itemData:clothingReleaseDatesUnformattedModel = clothingReleaseDatesUnformattedModel()
                    itemData = itemData.initload(item)
                    listReturnData.append(itemData)
                }
                closure(listReturnData,nil)
            }
            else {
                closure(nil,nil)
            }
        }
    }
    func getNews(closure: @escaping (_ response: [newsModel]?, _ error: Error?) -> Void) {
        requestSON("http://soleinsider.com/mobileapi/news", param: nil, method: .GET, loading: true) { (data, error) in
            if let data = data as? [[String: Any]] {
                var listReturnData:[newsModel] = [newsModel]()
                for item in data{
                    var itemData:newsModel = newsModel()
                    itemData = itemData.initload(item)
                    listReturnData.append(itemData)
                }
                closure(listReturnData,nil)
            }
            else {
                closure(nil,nil)
            }
        }
    }
}
