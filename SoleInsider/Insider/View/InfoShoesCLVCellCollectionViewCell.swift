//
//  InfoShoesCLVCellCollectionViewCell.swift
//  Insider
//
//  Created by admin on 12/07/2021.
//

import UIKit
import SwiftyShadow
import RealmSwift

extension UIView {
    var parentShoesViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
class InfoShoesCLVCellCollectionViewCell: UICollectionViewCell {
    var itemSendDitail:AddDicModel =  AddDicModel(name:"", stockx_thumbnail_url: "", release_date: "", price:"", changed:0, fav:0, direction:0, idpro: 0)
    var indexSend = -1
    @IBOutlet weak var infoImageShoes: UIImageView!
    @IBOutlet weak var labelNameShoes: UILabel!
    
    @IBOutlet weak var labelDateShoes: UILabel!
    
    @IBOutlet weak var labelPriceShoes: UILabel!
    
    @IBOutlet weak var textDecription: UITextView!
    
    @IBOutlet weak var testButtonLike: UIButton!
    
        
    @IBOutlet weak var buttonDetailMore: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()

        
            
        testButtonLike.layer.cornerRadius = 5
        testButtonLike.layer.shadowRadius = 10
        testButtonLike.layer.shadowOpacity = 0.5
        testButtonLike.layer.shadowColor = UIColor.white.cgColor
        testButtonLike.layer.shadowOffset = CGSize.zero
        testButtonLike.layer.borderWidth = 1.0
        testButtonLike.layer.borderColor = UIColor(rgb: 0xE0E2F2).cgColor




        buttonDetailMore.layer.cornerRadius = 5
        buttonDetailMore.layer.shadowRadius = 10
        buttonDetailMore.layer.shadowOpacity = 0.5
        buttonDetailMore.layer.shadowColor = UIColor.white.cgColor
        buttonDetailMore.layer.shadowOffset = CGSize.zero
        buttonDetailMore.layer.borderWidth = 1.0
        buttonDetailMore.layer.borderColor = UIColor(rgb: 0xE0E2F2).cgColor
    }

    
    @IBAction func FavoriteShoes(_ sender: UIButton) {
        if itemSendDitail.fav == 1{
            itemSendDitail.fav = 0
            testButtonLike.setImage(UIImage(named: "unlike.png"), for: .normal)
            SQLiteFavoriteShoes.shared.addFavoriteDatabase(itemSend:itemSendDitail) { (data, error) in
                if let _ = data{ //neu thanh cong
                    if let parent = self.parentShoesViewController as? InfoViewController{
                        if self.indexSend >= 0 {
                            parent.listFavouriteShoes[self.indexSend].fav = 0
                            parent.collectionViewMain.reloadData()
                        }
                    }
                }
            }
        }else{
            itemSendDitail.fav = 1
            testButtonLike.setImage(UIImage(named: "like.png"), for: .normal)
            SQLiteFavoriteShoes.shared.addFavoriteDatabase(itemSend:itemSendDitail) { (data, error) in
                if let _ = data{ //neu thanh cong
                    if (self.parentShoesViewController as? InfoViewController) != nil{
                        if let parent = self.parentShoesViewController as? InfoViewController{
                            if self.indexSend >= 0 {
                                parent.listFavouriteShoes[self.indexSend].fav = 1
                                parent.collectionViewMain.reloadData()
                            }
                        }
                    }
                }
            }
        }

    }

}
