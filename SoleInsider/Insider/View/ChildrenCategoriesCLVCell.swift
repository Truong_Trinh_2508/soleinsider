//
//  ChildrenCategoriesCLVCell.swift
//  Insider
//
//  Created by admin on 06/07/2021.
//

import UIKit

class ChildrenCategoriesCLVCell: UICollectionViewCell {
    @IBOutlet weak var labelName:UILabel!
    @IBOutlet weak var viewTong:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelName.isEnabled = false
        
        // Initialization code
    }
    
}
