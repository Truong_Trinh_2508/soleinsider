//
//  CategoriesMainCLVCell.swift
//  Insider
//
//  Created by admin on 06/07/2021.
//

import UIKit

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

class CategoriesMainCLVCell: UICollectionViewCell {
    var listCategories:[String] = ["Nike","Yeezy","Jordan","Adidas","Rebok","Puma"]
    @IBOutlet weak var collectionViewChildren: UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionViewChildren.register(UINib(nibName: ChildrenCategoriesCLVCell.className, bundle: nil), forCellWithReuseIdentifier: ChildrenCategoriesCLVCell.className)
    }

}
extension CategoriesMainCLVCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let parentVC = self.parentViewController as? MainViewController {
            parentVC.listData_Filter.removeAll()
            parentVC.searchBar.text = listCategories[indexPath.row].lowercased()
            parentVC.loadSearchPro(textSearch: listCategories[indexPath.row].lowercased())
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: UIScreen.main.bounds.width/6 - 10, height: 60)
        }
        return CGSize(width: UIScreen.main.bounds.width/3 - 10, height: 60)
    }
}

extension CategoriesMainCLVCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChildrenCategoriesCLVCell.className, for: indexPath) as! ChildrenCategoriesCLVCell
        cell.labelName.text = listCategories[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        return UICollectionReusableView()
    }
    
}

