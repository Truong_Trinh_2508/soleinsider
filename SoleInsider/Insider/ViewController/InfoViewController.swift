//
//  InfoViewController.swift
//  Insider
//
//  Created by admin on 14/07/2021.
//

import UIKit

class InfoViewController: UIViewController {
    @IBOutlet weak var collectionViewMain:UICollectionView!
    var stringFilter:String = ""
    var indexSelect = -1
    var listFavouriteShoes:[releaseDatesUnformattedModel] = [releaseDatesUnformattedModel]()
    var listSave:[releaseDatesUnformattedModel] = [releaseDatesUnformattedModel]()
    var listData_Filter:[releaseDatesUnformattedModel] = [releaseDatesUnformattedModel]()
    var isFilterAll:Bool = false
    var bRec:Bool = false
    var bRec1:Bool = false
    var bRec2:Bool = false
    var bRec3:Bool = false
    @IBOutlet weak var homeChangeImage: UIButton!
    @IBAction func homeChange(_ sender: UIButton) {
        if bRec == true {
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            bRec = false
        }else{
            homeChangeImage.setImage(UIImage(named: "home1.png"), for: .normal)
            let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
            let mainVC  = storyboard1.instantiateViewController(withIdentifier: MainViewController.className) as! MainViewController
            mainVC.modalPresentationStyle = .fullScreen
            self.present(mainVC, animated: true, completion: nil)
            bRec = false
            bRec1 = false
            bRec2 = false
            bRec3 = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
        }
    }
    @IBOutlet weak var favoriteChangeImage: UIButton!
    
    @IBAction func favoriteChange(_ sender: UIButton) {
        if bRec1 == true {
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            bRec1 = false
        }else{
            favoriteChangeImage.setImage(UIImage(named: "favorite1.png"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let FavTab = storyboard.instantiateViewController(withIdentifier: FavoriteViewController.className) as? FavoriteViewController
            FavTab!.modalPresentationStyle = .fullScreen
            self.present(FavTab!, animated: true, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
        }
    }
    @IBOutlet weak var newsChangeImage: UIButton!
    
    @IBAction func newsChange(_ sender: UIButton) {
        if bRec2 == true {
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            bRec2 = false
        }else{
            newsChangeImage.setImage(UIImage(named: "new1.png"), for: .normal)
            let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
            let newsTab = storyboard1.instantiateViewController(withIdentifier: NewsViewController.className) as? NewsViewController
            newsTab!.modalPresentationStyle = .fullScreen
            self.present(newsTab!, animated: true, completion: nil)
            bRec2 = false
            bRec1 = false
            bRec = false
            bRec3 = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
            
        }
    }
    @IBOutlet weak var saleChangeImage: UIButton!
    
    @IBAction func saleChange(_ sender: UIButton) {
        if bRec3 == true{
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
            bRec3 = false
        }else{
            saleChangeImage.setImage(UIImage(named: "sale1.png"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let saleTab = storyboard.instantiateViewController(withIdentifier: SaleViewController.className) as? SaleViewController
            saleTab!.modalPresentationStyle = .fullScreen
            self.present(saleTab!, animated: true, completion: nil)
            bRec3 = false
            bRec1 = false
            bRec2 = false
            bRec = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
        }
    }
    
    func getInfoShoes(andCompletion completion:@escaping (_ moviesResponse: [releaseDatesUnformattedModel], _ error: Error?) -> ()) {
        APIService.shared.getReleaseDatesUnformatted() { (response, error) in
            if let listData_Filter = response{
                for item in listData_Filter{
                    if item.name.contains(self.stringFilter) == true{
                        self.listData_Filter.append(item)
                    }
                }
                var listSave:[releaseDatesUnformattedModel] = [releaseDatesUnformattedModel]()
                SQLiteFavoriteShoes.shared.getDataDic() { (repond, error) in
                    if let repond = repond{
                        listSave = repond
                        for item in listSave {
                            var NextPro = 0
                            for item2 in self.listData_Filter {
                                if item.id  == item2.id {
                                    self.listData_Filter[NextPro].fav = item.fav
                                }
                                NextPro = NextPro + 1
                            }
                        }
                        DispatchQueue.main.async {
                            self.collectionViewMain.reloadData()
                        }
                    }
                }
            }
            completion(self.listData_Filter, error)
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewMain.register(UINib(nibName: InfoShoesCLVCellCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: InfoShoesCLVCellCollectionViewCell.className)
        self.getInfoShoes(){_,_ in
        }
        for item in listFavouriteShoes{
            if item.changed == 1{
                listSave.append(item)
            }
        }
    }
    
    @IBAction func Backmain(_ sender: Any) {
        self.dismiss(animated: true)
    }
}

extension InfoViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listData_Filter.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: InfoShoesCLVCellCollectionViewCell.className, for: indexPath) as! InfoShoesCLVCellCollectionViewCell
        let itemSend:AddDicModel = AddDicModel(name:"", stockx_thumbnail_url: "", release_date: "", price:"", changed:0, fav:0, direction:0, idpro: 0)
        itemSend.idpro = Int(listData_Filter[indexPath.row].id) ?? 0
        itemSend.name = listData_Filter[indexPath.row].name
        itemSend.stockx_thumbnail_url = listData_Filter[indexPath.row].stockx_thumbnail_url
        itemSend.release_date = listData_Filter[indexPath.row].release_date
        itemSend.price = listData_Filter[indexPath.row].price
        itemSend.fav = listData_Filter[indexPath.row].fav
        let url = URL(string: listData_Filter[indexPath.row].stockx_thumbnail_url)
        cell.itemSendDitail = itemSend
        cell.infoImageShoes.kf.setImage(with: url)
        cell.labelNameShoes.text = listData_Filter[indexPath.row].name
        cell.labelDateShoes.text = "Release date:" + listData_Filter[indexPath.row].release_date
        cell.labelPriceShoes.text = "Price:$" + listData_Filter[indexPath.row].price
        if itemSend.fav == 0{
            cell.testButtonLike.imageView?.image = UIImage.init(named: "undislike")
        }else{
            cell.testButtonLike.imageView?.image = UIImage.init(named: "unlike")
        }
        if let attributedData = self.listData_Filter[indexPath.row].description1.htmlAttributedString{
            cell.textDecription.attributedText = attributedData
        }
        cell.textDecription.isEditable = false
        cell.textDecription.textColor = UIColor.white
        cell.textDecription.font = UIFont.systemFont(ofSize: 15.0)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        return UICollectionReusableView()
    }
    
}

extension InfoViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: UIScreen.main.bounds.width, height: 800)
        }
        return CGSize(width: UIScreen.main.bounds.width, height: 754)
    }
}
extension String {
    var htmlAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
