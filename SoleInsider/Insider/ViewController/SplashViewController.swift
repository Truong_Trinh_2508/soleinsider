//
//  SplashViewController.swift
//  Insider
//
//  Created by admin on 06/07/2021.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        var timeCount = 0
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
            timeCount = timeCount + 1
            if timeCount == 2 {
                let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                secondVC.modalPresentationStyle = .fullScreen
                self.present(secondVC, animated: true, completion: nil)
                timer.invalidate()
            }
        }
    }

}
