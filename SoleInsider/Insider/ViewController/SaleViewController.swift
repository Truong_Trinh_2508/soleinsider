//
//  SaleViewController.swift
//  Insider
//
//  Created by admin on 21/07/2021.
//

import UIKit

class SaleViewController: UIViewController,UISearchBarDelegate {


    @IBOutlet weak var saleCollectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!

    var listDataSale:[clothingReleaseDatesUnformattedModel] = [clothingReleaseDatesUnformattedModel]()
    var listDataSale_Filter:[clothingReleaseDatesUnformattedModel] = [clothingReleaseDatesUnformattedModel]()
    var isFilterAll:Bool = false

    var bRec:Bool = false
    var bRec1:Bool = false
    var bRec2:Bool = false
    var bRec3:Bool = false


    @IBOutlet weak var homeChangeImage: UIButton!
    @IBAction func homeChange(_ sender: UIButton) {

        if bRec == true {
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            bRec = false
        }else{
            homeChangeImage.setImage(UIImage(named: "home1.png"), for: .normal)
            let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
            let mainVC  = storyboard1.instantiateViewController(withIdentifier: MainViewController.className) as! MainViewController
            mainVC.modalPresentationStyle = .fullScreen
            self.present(mainVC, animated: true, completion: nil)
            bRec = false
            bRec1 = false
            bRec2 = false
            bRec3 = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
            
        }
    }
    @IBOutlet weak var favoriteChangeImage: UIButton!
    
    @IBAction func favoriteChange(_ sender: UIButton) {
        if bRec1 == true {
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            bRec1 = false
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let FavTab = storyboard.instantiateViewController(withIdentifier: FavoriteViewController.className) as? FavoriteViewController
            FavTab!.modalPresentationStyle = .fullScreen
            self.present(FavTab!, animated: true, completion: nil)
            favoriteChangeImage.setImage(UIImage(named: "favorite1.png"), for: .normal)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
        }
    }
    @IBOutlet weak var newsChangeImage: UIButton!
        @IBAction func newsChange(_ sender: UIButton){
        if bRec2 == true {
    
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            bRec2 = false
        }else{
            newsChangeImage.setImage(UIImage(named: "new1.png"), for: .normal)
            let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
            let newsTab = storyboard1.instantiateViewController(withIdentifier: NewsViewController.className) as? NewsViewController
            newsTab!.modalPresentationStyle = .fullScreen
            self.present(newsTab!, animated: true, completion: nil)
            bRec2 = false
            bRec1 = false
            bRec = false
            bRec3 = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)

        }
    }
    @IBOutlet weak var saleChangeImage: UIButton!
        @IBAction func saleChange(_ sender: UIButton) {
            if bRec3 == true{
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
            bRec3 = false
        }else{
            saleChangeImage.setImage(UIImage(named: "sale1.png"), for: .normal)
//            if let saleVC = MainViewController.shared.saleTab{
//                saleVC.modalPresentationStyle = .fullScreen
//                self.present(saleVC, animated: true, completion: nil)
//            }else{
//                let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
//                MainViewController.shared.saleTab = storyboard1.instantiateViewController(withIdentifier: SaleViewController.className) as? SaleViewController
//                MainViewController.shared.saleTab!.modalPresentationStyle = .fullScreen
//                self.present(MainViewController.shared.saleTab!, animated: true, completion: nil)
//            }
            bRec3 = false
            bRec1 = false
            bRec2 = false
            bRec = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
        }
    }
    @IBAction func SettingButton(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        saleCollectionView.delegate = self
        saleCollectionView.dataSource = self
        searchBar.delegate = self
        
        saleCollectionView.register(UINib(nibName: releaseDatesCLVCell.className, bundle: nil), forCellWithReuseIdentifier: releaseDatesCLVCell.className)
        saleCollectionView.register(UINib(nibName: InfoSaleViewController.className, bundle: nil), forCellWithReuseIdentifier: InfoSaleViewController.className)
        saleCollectionView.register(UINib(nibName: InfoShoesCLVCellCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: InfoShoesCLVCellCollectionViewCell.className)
        
        APIService.shared.getClothingReleaseDatesUnformatted(){ response, error in
            if let response = response{
                self.listDataSale = response
                self.saleCollectionView.reloadData()
            }
        }
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isFilterAll = true;
     }

     func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isFilterAll = false;
     }

     func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isFilterAll = false;
     }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isFilterAll = false;
     }
    func loadSearchPro(textSearch:String){
        listDataSale_Filter = listDataSale.filter({ (anArray) -> Bool in
            guard let firstString = (anArray.name as? String) else{
               return false
            }
            let tmp: NSString = firstString as NSString
            let range = tmp.range(of: textSearch, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
         if(listDataSale_Filter.count == 0){
            isFilterAll = false;
         } else {
            isFilterAll = true;
         }
         self.saleCollectionView.reloadData()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        listDataSale_Filter = listDataSale.filter({ (anArray) -> Bool in
            guard let firstString = (anArray.name as? String) else{
               return false
            }
            let tmp: NSString = firstString as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
         if(listDataSale_Filter.count == 0){
            isFilterAll = false;
         } else {
            isFilterAll = true;
         }
         self.saleCollectionView.reloadData()
     }

     override func didReceiveMemoryWarning() {
         super.didReceiveMemoryWarning()
         // Dispose of any resources that can be recreated.
     }


     func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
         return 1
     }
}


extension SaleViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InfoSaleViewController") as! InfoSaleViewController
        vc.modalPresentationStyle = .fullScreen
        if isFilterAll == false{
            vc.stringFilter = listDataSale[indexPath.row].name
        }else{
            vc.stringFilter = listDataSale_Filter[indexPath.row].name
        }
        self.present(vc, animated: true, completion: nil)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isFilterAll == false{
            return listDataSale.count
        }
        return listDataSale_Filter.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: releaseDatesCLVCell.className, for: indexPath) as! releaseDatesCLVCell
        if isFilterAll == false{
            let url = URL(string: listDataSale[indexPath.row].stockx_thumbnail_url)
            cell.imageCover.kf.setImage(with: url)
            cell.labelName.text = listDataSale[indexPath.row].name
            cell.labelTitle.text = "Release date:" + listDataSale[indexPath.row].release_date
            cell.labelPrice.text = "Price:$" + listDataSale[indexPath.row].price
            
        }else{
            let url = URL(string: listDataSale_Filter[indexPath.row].stockx_thumbnail_url)
            cell.imageCover.kf.setImage(with: url)
            cell.labelName.text = listDataSale_Filter[indexPath.row].name
            cell.labelTitle.text = "Release date:" + listDataSale_Filter[indexPath.row].release_date
            cell.labelPrice.text = "Price:$" + listDataSale_Filter[indexPath.row].price
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        return UICollectionReusableView()
    }
    
}

extension SaleViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: UIScreen.main.bounds.width - 100, height: 200)
        }
        return CGSize(width: UIScreen.main.bounds.width, height: 200)
    }
    
}
