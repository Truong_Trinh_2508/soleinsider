//
//  MainViewController.swift
//  Insider
//
//  Created by admin on 05/07/2021.
//

import UIKit
import Kingfisher
extension UISearchBar {
    
    var textColor:UIColor? {
        get {
            if let textField = self.value(forKey: "searchField") as?
                UITextField  {
                return textField.textColor
            } else {
                return nil
            }
        }
        
        set (newValue) {
            if let textField = self.value(forKey: "searchField") as?
                UITextField  {
                textField.textColor = newValue
            }
        }
    }
}
extension NSObject {
    func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
    var className: String {
        return String(describing: type(of: self))
    }
    class var className: String {
        return String(describing: self)
    }
}

class MainViewController: UIViewController ,UISearchBarDelegate {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    static var shared: MainViewController = MainViewController()
    
    var listDataHome:[releaseDatesUnformattedModel] = [releaseDatesUnformattedModel]()
    var bRec:Bool = false
    var bRec1:Bool = false
    var bRec2:Bool = false
    var bRec3:Bool = false
    var listData_Filter:[releaseDatesUnformattedModel] = [releaseDatesUnformattedModel]()
    var isFilterAll:Bool = false
    
    @IBOutlet weak var homeChangeImage: UIButton!
    @IBAction func homeChange(_ sender: UIButton) {
        if bRec == true {
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            bRec = false
        }else{
            homeChangeImage.setImage(UIImage(named: "home1.png"), for: .normal)
            bRec = false
            bRec1 = false
            bRec2 = false
            bRec3 = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
        }
    }
    @IBOutlet weak var favoriteChangeImage: UIButton!
    
    @IBAction func favoriteChange(_ sender: UIButton) {
        if bRec1 == true {
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            bRec1 = false
        }else{
            favoriteChangeImage.setImage(UIImage(named: "favorite1.png"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let FavTab = storyboard.instantiateViewController(withIdentifier: FavoriteViewController.className) as? FavoriteViewController
            FavTab!.modalPresentationStyle = .fullScreen
            self.present(FavTab!, animated: true, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
        }
    }
    @IBOutlet weak var newsChangeImage: UIButton!
    
    @IBAction func newsChange(_ sender: UIButton) {
        if bRec2 == true {
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            bRec2 = false
        }else{
            newsChangeImage.setImage(UIImage(named: "new1.png"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let newsTab = storyboard.instantiateViewController(withIdentifier: NewsViewController.className) as? NewsViewController
            newsTab!.modalPresentationStyle = .fullScreen
            self.present(newsTab!, animated: true, completion: nil)
            bRec2 = false
            bRec1 = false
            bRec = false
            bRec3 = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
            
        }
    }
    @IBOutlet weak var saleChangeImage: UIButton!
    
    @IBAction func saleChange(_ sender: UIButton) {
        if bRec3 == true{
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
            bRec3 = false
        }else{
            saleChangeImage.setImage(UIImage(named: "sale1.png"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let saleTab = storyboard.instantiateViewController(withIdentifier: SaleViewController.className) as? SaleViewController
            saleTab!.modalPresentationStyle = .fullScreen
            self.present(saleTab!, animated: true, completion: nil)
            bRec3 = false
            bRec1 = false
            bRec2 = false
            bRec = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
        }
    }
    
    
    @IBAction func SettingButton(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        MainViewController.shared = self
        collectionView.delegate = self
        collectionView.dataSource = self
        searchBar.delegate = self
        searchBar.textColor = UIColor.white
        collectionView.register(UINib(nibName: CategoriesMainCLVCell.className, bundle: nil), forCellWithReuseIdentifier: CategoriesMainCLVCell.className)
        collectionView.register(UINib(nibName: releaseDatesCLVCell.className, bundle: nil), forCellWithReuseIdentifier: releaseDatesCLVCell.className)
        collectionView.register(UINib(nibName: InfoViewController.className, bundle: nil), forCellWithReuseIdentifier: InfoViewController.className)
        
        APIService.shared.getReleaseDatesUnformatted(){ response, error in
            if let response = response{
                self.listDataHome = response
                var listSave: [releaseDatesUnformattedModel] =  [releaseDatesUnformattedModel]()
                SQLiteFavoriteShoes.shared.getDataDic() { (repond, error) in
                    if let repond = repond{
                        listSave = repond
                        for item in listSave {
                            var NextPro = 0
                            for item2 in self.listDataHome {
                                if item.id  == item2.id {
                                    self.listDataHome[NextPro].fav = item.fav
                                }
                                NextPro = NextPro + 1
                            }
                        }
                        self.collectionView.reloadData()
                    }
                }
            }
        }
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isFilterAll = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isFilterAll = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isFilterAll = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isFilterAll = false;
    }
    func loadSearchPro(textSearch:String){
        listData_Filter = listDataHome.filter({ (anArray) -> Bool in
            guard let firstString = (anArray.name as? String) else{
                return false
            }
            let tmp: NSString = firstString as NSString
            let range = tmp.range(of: textSearch, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        if(listData_Filter.count == 0){
            isFilterAll = false;
        } else {
            isFilterAll = true;
        }
        self.collectionView.reloadData()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        listData_Filter = listDataHome.filter({ (anArray) -> Bool in
            guard let firstString = (anArray.name as? String) else{
                return false
            }
            let tmp: NSString = firstString as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        if(listData_Filter.count == 0){
            isFilterAll = false;
        } else {
            isFilterAll = true;
        }
        self.collectionView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSectionsInTableView(collectionView: UICollectionView) -> Int {
        return 1 
    }
    
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        if isFilterAll == false{
            return listDataHome.count
        }
        return listData_Filter.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InfoViewController") as! InfoViewController
        vc.modalPresentationStyle = .fullScreen
        if isFilterAll == false{
            vc.stringFilter = listDataHome[indexPath.row].name
        }else{
            vc.stringFilter = listData_Filter[indexPath.row].name
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoriesMainCLVCell.className, for: indexPath) as! CategoriesMainCLVCell
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: releaseDatesCLVCell.className, for: indexPath) as! releaseDatesCLVCell
        if listDataHome[indexPath.row].fav == 0{
            cell.buttonFav.image = UIImage.init(named: "")
        }else{
            cell.buttonFav.image = UIImage.init(named: "unlike")
        }
        if isFilterAll == false{
            let url = URL(string: listDataHome[indexPath.row].stockx_thumbnail_url)
            cell.imageCover.kf.setImage(with: url)
            cell.imageCover.layer.cornerRadius = 10
            cell.imageCover.layer.masksToBounds = true
            cell.imageCover.clipsToBounds = true
            cell.labelName.text = listDataHome[indexPath.row].name
            cell.labelTitle.text = "Release date:" + listDataHome[indexPath.row].release_date
            cell.labelPrice.text = "Price:$" + listDataHome[indexPath.row].price
            
        }else{
            cell.imageCover.layer.cornerRadius = 10
            cell.imageCover.layer.masksToBounds = true
            cell.imageCover.clipsToBounds = true
            let url = URL(string: listData_Filter[indexPath.row].stockx_thumbnail_url)
            cell.imageCover.kf.setImage(with: url)
            cell.labelName.text = listData_Filter[indexPath.row].name
            cell.labelTitle.text = "Release date:" + listData_Filter[indexPath.row].release_date
            cell.labelPrice.text = "Price:$" + listData_Filter[indexPath.row].price
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        return UICollectionReusableView()
    }
    
    
}

extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0{
            if UIDevice.current.userInterfaceIdiom == .pad{
                return CGSize(width: UIScreen.main.bounds.width, height: 100)
            }
            return CGSize(width: UIScreen.main.bounds.width, height: 100)
        }
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: UIScreen.main.bounds.width, height: 200)
        }
        return CGSize(width: UIScreen.main.bounds.width, height: 200)
    }
    
}


extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
