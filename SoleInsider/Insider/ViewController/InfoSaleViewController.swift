//
//  InfoSaleViewController.swift
//  Insider
//
//  Created by admin on 06/08/2021.
//

import UIKit

class InfoSaleViewController: UIViewController {
    @IBOutlet weak var infoSaleCollectionViewMain:UICollectionView!
    var stringFilter:String = ""
    var indexSelect = -1
    var listFavoriteShoes:[AddDicModel] = [AddDicModel]()
    var listSave:[AddDicModel] = [AddDicModel]()
    var listDataSale_Filter:[clothingReleaseDatesUnformattedModel] = [clothingReleaseDatesUnformattedModel]()

    var bRec:Bool = false
    var bRec1:Bool = false
    var bRec2:Bool = false
    var bRec3:Bool = false
    @IBOutlet weak var homeChangeImage: UIButton!
    @IBAction func homeChange(_ sender: UIButton) {
        
        if bRec == true {
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            bRec = false
        }else{
            homeChangeImage.setImage(UIImage(named: "home1.png"), for: .normal)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            bRec = false
            bRec1 = false
            bRec2 = false
            bRec3 = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
            
        }
    }
    @IBOutlet weak var favoriteChangeImage: UIButton!
    @IBAction func favoriteChange(_ sender: UIButton) {
        if bRec1 == true {
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            bRec1 = false
        }else{
            favoriteChangeImage.setImage(UIImage(named: "favorite1.png"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let FavTab = storyboard.instantiateViewController(withIdentifier: FavoriteViewController.className) as? FavoriteViewController
            FavTab!.modalPresentationStyle = .fullScreen
            self.present(FavTab!, animated: true, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
        }
    }
    @IBOutlet weak var newsChangeImage: UIButton!
    @IBAction func newsChange(_ sender: UIButton) {
        if bRec2 == true {
    
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            bRec2 = false
        }else{
            newsChangeImage.setImage(UIImage(named: "new1.png"), for: .normal)
            let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
            let newsTab = storyboard1.instantiateViewController(withIdentifier: NewsViewController.className) as? NewsViewController
            newsTab!.modalPresentationStyle = .fullScreen
            self.present(newsTab!, animated: true, completion: nil)
            bRec2 = false
            bRec1 = false
            bRec = false
            bRec3 = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)

        }
    }
    
    @IBOutlet weak var saleChangeImage: UIButton!
    @IBAction func saleChange(_ sender: UIButton) {
        if bRec3 == true{
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
            bRec3 = false
        }else{
            saleChangeImage.setImage(UIImage(named: "sale1.png"), for: .normal)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SaleViewController") as! SaleViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            bRec3 = false
            bRec1 = false
            bRec2 = false
            bRec = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
        }
    }

    func getInfoSale(andCompletion completion:@escaping (_ moviesResponse: [clothingReleaseDatesUnformattedModel], _ error: Error?) -> ()) {
        APIService.shared.getClothingReleaseDatesUnformatted() { (response, error) in
            if let listDataSale_Filter = response{
                for item in listDataSale_Filter{
                    if item.name.contains(self.stringFilter) == true{
                        self.listDataSale_Filter.append(item)
                    }
                }
                DispatchQueue.main.async {
                    self.infoSaleCollectionViewMain.reloadData()
                }
            }
            completion(self.listDataSale_Filter, error)
        }
    }


    
    override func viewDidLoad() {
        super.viewDidLoad()
        infoSaleCollectionViewMain.register(UINib(nibName: InfoShoesCLVCellCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: InfoShoesCLVCellCollectionViewCell.className)
        self.getInfoSale(){_,_ in
               }


    }
    
    @IBAction func Backmain(_ sender: Any) {
        self.dismiss(animated: true)
    }
}

extension InfoSaleViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listDataSale_Filter.count
    }
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: InfoShoesCLVCellCollectionViewCell.className, for: indexPath) as! InfoShoesCLVCellCollectionViewCell
        let itemSend:AddDicModel = AddDicModel(name:"", stockx_thumbnail_url: "", release_date: "", price:"", changed:0, fav:0, direction:0, idpro: 0)
        itemSend.idpro = Int(listDataSale_Filter[indexPath.row].id) ?? 1
        let url = URL(string: listDataSale_Filter[indexPath.row].stockx_thumbnail_url)
        cell.infoImageShoes.kf.setImage(with: url)
        cell.labelNameShoes.text = listDataSale_Filter[indexPath.row].name
        cell.labelDateShoes.text = "Release date:" + listDataSale_Filter[indexPath.row].release_date
        cell.labelPriceShoes.text = "Price:$" + listDataSale_Filter[indexPath.row].price
        if let attributedData = self.listDataSale_Filter[indexPath.row].content.htmlAttributedString{
            cell.textDecription.attributedText = attributedData
        }
        
        cell.textDecription.isEditable = false
        cell.textDecription.textColor = UIColor.white
        cell.textDecription.font = UIFont.systemFont(ofSize: 15.0)
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        return UICollectionReusableView()
    }
    
}

extension InfoSaleViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: UIScreen.main.bounds.width, height: infoSaleCollectionViewMain.frame.height)
        }
        return CGSize(width: UIScreen.main.bounds.width, height: infoSaleCollectionViewMain.frame.height)
    }
}
//extension String {
//    var htmlAttributedString: NSAttributedString? {
//        guard let data = data(using: .utf8) else { return nil }
//        do {
//            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
//        } catch {
//            return nil
//        }
//    }
//    var htmlString: String {
//        return htmlToAttributedString?.string ?? ""
//    }
//}
