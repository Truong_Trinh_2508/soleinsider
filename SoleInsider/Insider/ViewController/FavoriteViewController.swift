//
//  FavoriteViewController.swift
//  Insider
//
//  Created by admin on 14/07/2021.
//

import UIKit
import SwiftyShadow
import SwiftOverlayShims
import RealmSwift

extension NSObject{
    func backgrounds(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
    var classNames: String {
        return String(describing: type(of: self))
    }
    class var classNames: String {
        return String(describing: self)
    }
}
class FavoriteViewController: UIViewController {
    @IBOutlet weak var FavCollectionView: UICollectionView!
    var bRec:Bool = false
    var bRec1:Bool = false
    var bRec2:Bool = false
    var bRec3:Bool = false
    var listFavoriteShoes:[releaseDatesUnformattedModel] = [releaseDatesUnformattedModel]()
    var saveDataFavoriteShoes:[releaseDatesUnformattedModel] = [releaseDatesUnformattedModel]()
    var listDataOld = [releaseDatesUnformattedModel]()
    @IBOutlet weak var homeChangeImage: UIButton!
    @IBAction func homeChange(_ sender: UIButton) {
        
        if bRec == true {
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            bRec = false
        }else{
            homeChangeImage.setImage(UIImage(named: "home1.png"), for: .normal)
            let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
            let mainVC  = storyboard1.instantiateViewController(withIdentifier: MainViewController.className) as! MainViewController
            mainVC.modalPresentationStyle = .fullScreen
            self.present(mainVC, animated: true, completion: nil)
            bRec = false
            bRec1 = false
            bRec2 = false
            bRec3 = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
            
        }
    }
    @IBOutlet weak var favoriteChangeImage: UIButton!
    
    @IBAction func favoriteChange(_ sender: UIButton) {
        if bRec1 == true {
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            bRec1 = false
        }else{
            favoriteChangeImage.setImage(UIImage(named: "favorite1.png"), for: .normal)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
        }
    }
    @IBOutlet weak var newsChangeImage: UIButton!
    @IBAction func newsChange(_ sender: UIButton){
        if bRec2 == true {
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            bRec2 = false
        }else{
            newsChangeImage.setImage(UIImage(named: "new1.png"), for: .normal)
            let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
            let newsTab = storyboard1.instantiateViewController(withIdentifier: NewsViewController.className) as? NewsViewController
            newsTab!.modalPresentationStyle = .fullScreen
            self.present(newsTab!, animated: true, completion: nil)
            bRec2 = false
            bRec1 = false
            bRec = false
            bRec3 = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
        }
    }
    @IBOutlet weak var saleChangeImage: UIButton!
    @IBAction func saleChange(_ sender: UIButton) {
        if bRec3 == true{
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
            bRec3 = false
        }else{
            saleChangeImage.setImage(UIImage(named: "sale1.png"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let saleTab = storyboard.instantiateViewController(withIdentifier: SaleViewController.className) as? SaleViewController
            saleTab!.modalPresentationStyle = .fullScreen
            self.present(saleTab!, animated: true, completion: nil)
            bRec3 = false
            bRec1 = false
            bRec2 = false
            bRec = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
        }
    }
    
    
    @IBAction func SettingButton(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        FavCollectionView.delegate = self
        FavCollectionView.dataSource = self
        FavCollectionView.register(UINib(nibName: releaseDatesCLVCell.className, bundle: nil), forCellWithReuseIdentifier: releaseDatesCLVCell.className)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        saveDataFavoriteShoes.removeAll()
        FavCollectionView.backgroundColor = UIColor.clear
        SQLiteFavoriteShoes.shared.getDataDic() { (repond, error) in
            if let repond = repond{
                self.saveDataFavoriteShoes = repond
                self.listFavoriteShoes.removeAll()
                for item in self.saveDataFavoriteShoes {
                    if item.fav == 1 {
                        self.listFavoriteShoes.append(item)
                    }
                }
                self.listDataOld = self.listFavoriteShoes
                self.FavCollectionView.reloadData()
            }
        }
        self.FavCollectionView.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        DispatchQueue.main.async {
            self.FavCollectionView.reloadData()
        }
    }
    @IBAction func btnUpDateFavoriteShoes(_ sender: Any) {
        
        var listFavoriteOffline:[releaseDatesUnformattedModel] = [releaseDatesUnformattedModel]()
        for item in listFavoriteShoes{
            if item.fav == 1{
                listFavoriteOffline.append(item)
            }
        }
        listFavoriteShoes = listFavoriteOffline
        FavCollectionView.reloadData()
    }
}
extension FavoriteViewController : UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listFavoriteShoes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: releaseDatesCLVCell.className, for: indexPath) as! releaseDatesCLVCell
        let url = URL(string: listFavoriteShoes[indexPath.row].stockx_thumbnail_url)
        cell.imageCover.kf.setImage(with: url)
        cell.labelName.text = listFavoriteShoes[indexPath.row].name
        cell.labelTitle.text = "Release date:" + listFavoriteShoes[indexPath.row].release_date
        cell.labelPrice.text = "Price:$" + listFavoriteShoes[indexPath.row].price
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InfoViewController") as! InfoViewController
        vc.modalPresentationStyle = .fullScreen
        vc.stringFilter = listFavoriteShoes[indexPath.row].name
        self.present(vc, animated: true, completion: nil)
    }
    
}
extension FavoriteViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0{
            if UIDevice.current.userInterfaceIdiom == .pad{
                return CGSize(width: UIScreen.main.bounds.width, height: 200)
            }
            return CGSize(width: UIScreen.main.bounds.width, height: 200)
        }
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: UIScreen.main.bounds.width, height: 200)
        }
        return CGSize(width: UIScreen.main.bounds.width, height: 200)
    }
    
}
