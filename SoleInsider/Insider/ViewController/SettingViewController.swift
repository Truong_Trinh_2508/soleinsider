//
//  SettingViewController.swift
//  Insider
//
//  Created by admin on 18/07/2021.
//

import UIKit

class SettingViewController: UIViewController {
    var bRec:Bool = false
    var bRec1:Bool = false
    var bRec2:Bool = false
    var bRec3:Bool = false
    var listNews_filter:[newsModel] = [newsModel]()
    var stringFilter:String = ""
    var descriptionString: String = ""
    @IBOutlet weak var imagePro: UIImageView!
    
    @objc func imageTappedRate(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            print("UIImageView tapped")
        }
    }
    @IBOutlet weak var homeChangeImage: UIButton!
    @IBAction func homeChange(_ sender: UIButton) {
        
        if bRec == true {
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            bRec = false
        }else{
            homeChangeImage.setImage(UIImage(named: "home1.png"), for: .normal)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            bRec = false
            bRec1 = false
            bRec2 = false
            bRec3 = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
        }
    }
    @IBOutlet weak var favoriteChangeImage: UIButton!
    
    @IBAction func favoriteChange(_ sender: UIButton) {
        if bRec1 == true {
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            bRec1 = false
        }else{
            favoriteChangeImage.setImage(UIImage(named: "favorite1.png"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let FavTab = storyboard.instantiateViewController(withIdentifier: FavoriteViewController.className) as? FavoriteViewController
            FavTab!.modalPresentationStyle = .fullScreen
            self.present(FavTab!, animated: true, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
        }
    }
    @IBOutlet weak var newsChangeImage: UIButton!
    @IBAction func newsChange(_ sender: UIButton){
        if bRec2 == true {
            
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            bRec2 = false
        }else{
            newsChangeImage.setImage(UIImage(named: "new1.png"), for: .normal)
            let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
            let newsTab = storyboard1.instantiateViewController(withIdentifier: NewsViewController.className) as? NewsViewController
            newsTab!.modalPresentationStyle = .fullScreen
            self.present(newsTab!, animated: true, completion: nil)
            bRec2 = false
            bRec1 = false
            bRec = false
            bRec3 = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
            
        }
    }
    @IBOutlet weak var saleChangeImage: UIButton!
    @IBAction func saleChange(_ sender: UIButton) {
        if bRec3 == true{
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
            bRec3 = false
        }else{
            saleChangeImage.setImage(UIImage(named: "sale1.png"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let saleTab = storyboard.instantiateViewController(withIdentifier: SaleViewController.className) as? SaleViewController
            saleTab!.modalPresentationStyle = .fullScreen
            self.present(saleTab!, animated: true, completion: nil)
            bRec3 = false
            bRec1 = false
            bRec2 = false
            bRec = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGR = UITapGestureRecognizer(target: self, action: #selector(self.imageTappedRate))
        imagePro.addGestureRecognizer(tapGR)
        imagePro.isUserInteractionEnabled = true
    }
    
    
    
}
