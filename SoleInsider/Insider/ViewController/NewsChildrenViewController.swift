//
//  NewsChildrenViewController.swift
//  Insider
//
//  Created by admin on 15/07/2021.
//

import UIKit

class NewsChildrenViewController: UIViewController, passDataToNewChildrenVC {
    func passDescription(_ description: String) {
        self.descriptionString = description
    }
    
   
    @IBOutlet weak var textViewNews: UITextView!
    var bRec:Bool = false
    var bRec1:Bool = false
    var bRec2:Bool = false
    var bRec3:Bool = false
    var descriptionString: String = ""


    @IBOutlet weak var homeChangeImage: UIButton!
    @IBAction func homeChange(_ sender: UIButton) {

        if bRec == true {
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            bRec = false
        }else{
            homeChangeImage.setImage(UIImage(named: "home1.png"), for: .normal)
            let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
            let mainVC  = storyboard1.instantiateViewController(withIdentifier: MainViewController.className) as! MainViewController
            mainVC.modalPresentationStyle = .fullScreen
            self.present(mainVC, animated: true, completion: nil)
            bRec = false
            bRec1 = false
            bRec2 = false
            bRec3 = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
            
        }
    }
    @IBOutlet weak var favoriteChangeImage: UIButton!
    
    @IBAction func favoriteChange(_ sender: UIButton) {
        if bRec1 == true {
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            bRec1 = false
        }else{
            favoriteChangeImage.setImage(UIImage(named: "favorite1.png"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let FavTab = storyboard.instantiateViewController(withIdentifier: FavoriteViewController.className) as? FavoriteViewController
            FavTab!.modalPresentationStyle = .fullScreen
            self.present(FavTab!, animated: true, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
        }
    }
    @IBOutlet weak var newsChangeImage: UIButton!
        @IBAction func newsChange(_ sender: UIButton){
        if bRec2 == true {
    
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            bRec2 = false
        }else{
            newsChangeImage.setImage(UIImage(named: "new1.png"), for: .normal)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewsViewController") as! NewsViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            bRec2 = false
            bRec1 = false
            bRec = false
            bRec3 = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)

        }
    }
    @IBOutlet weak var saleChangeImage: UIButton!
        @IBAction func saleChange(_ sender: UIButton) {
            if bRec3 == true{
            saleChangeImage.setImage(UIImage(named: "sale.png"), for: .normal)
            bRec3 = false
        }else{
            saleChangeImage.setImage(UIImage(named: "sale1.png"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let saleTab = storyboard.instantiateViewController(withIdentifier: SaleViewController.className) as? SaleViewController
            saleTab!.modalPresentationStyle = .fullScreen
            self.present(saleTab!, animated: true, completion: nil)
            bRec3 = false
            bRec1 = false
            bRec2 = false
            bRec = false
            favoriteChangeImage.setImage(UIImage(named: "favorite.png"), for: .normal)
            newsChangeImage.setImage(UIImage(named: "new.png"), for: .normal)
            homeChangeImage.setImage(UIImage(named: "home.png"), for: .normal)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let attributedData = self.descriptionString.htmlToAttributedString{
            self.textViewNews.attributedText = attributedData
        }
        textViewNews.isEditable = false
        textViewNews.textColor = UIColor.white
    }
    @IBAction func Backmain(_ sender: Any) {
        self.dismiss(animated: true)
    }

}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
