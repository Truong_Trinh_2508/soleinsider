//
//  clothingReleaseDatesUnformattedModel.swift
//  Insider
//
//  Created by admin on 05/07/2021.
//

import UIKit

class clothingReleaseDatesUnformattedModel: NSObject {
    var id:String = ""
    var name:String = ""
    var sku:String = ""
    var image:String = ""
    var link:String = ""
    var colorway:String = ""
    var description1:String = ""
    var content:String = ""
    var slug:String = ""
    var price:String = ""
    var coming_soon:String = ""
    var views:String = ""
    var resale:String = ""
    var type:String = ""
    var stockx_url:String = ""
    var stockx_thumbnail_url:String = ""
    var stockx_ticker_symbol:String = ""
    var stockx_name:String = ""
    var stockx_make:String = ""
    var stockx_model:String = ""
    var stockx_price:String = ""
    var stockx_highest_bid:String = ""
    var stockx_total_dollars:String = ""
    var stockx_lowest_ask:String = ""
    var stockx_last_sale:String = ""
    var stockx_deadstock_sold:String = ""
    var stockx_sales_last_72:String = ""
    var created_at:String = ""
    var updated_at:String = ""
    var product_id:String = ""
    var release_date_calendar:String = ""
    var release_date:String = ""
    var yes_votes:String = ""
    var no_votes:String = ""
    var total_votes:String = ""
    var yes_percentage:String = ""
    var no_percentage:String = ""
    
    func initload(_ json: [String:Any]) -> clothingReleaseDatesUnformattedModel {
        if let temp = json["id"] as? String { id = temp }
        if let temp = json["name"] as? String { name = temp }
        if let temp = json["sku"] as? String { sku = temp }
        if let temp = json["image"] as? String { image = temp }
        if let temp = json["link"] as? String { link = temp }
        if let temp = json["colorway"] as? String { colorway = temp }
        if let temp = json["description"] as? String { description1 = temp }
        if let temp = json["content"] as? String { content = temp }
        if let temp = json["slug"] as? String { slug = temp }
        if let temp = json["price"] as? String { price = temp }
        if let temp = json["coming_soon"] as? String { coming_soon = temp }
        if let temp = json["views"] as? String { views = temp }
        if let temp = json["resale"] as? String { resale = temp }
        if let temp = json["type"] as? String { type = temp }
        if let temp = json["stockx_url"] as? String { stockx_url = temp }
        if let temp = json["stockx_thumbnail_url"] as? String { stockx_thumbnail_url = temp }
        if let temp = json["stockx_ticker_symbol"] as? String { stockx_ticker_symbol = temp }
        if let temp = json["stockx_name"] as? String { stockx_name = temp }
        if let temp = json["stockx_make"] as? String { stockx_make = temp }
        if let temp = json["stockx_model"] as? String { stockx_model = temp }
        if let temp = json["stockx_price"] as? String { stockx_price = temp }
        if let temp = json["stockx_highest_bid"] as? String { stockx_highest_bid = temp }
        if let temp = json["stockx_total_dollars"] as? String { stockx_total_dollars = temp }
        if let temp = json["stockx_lowest_ask"] as? String { stockx_lowest_ask = temp }
        if let temp = json["stockx_last_sale"] as? String { stockx_last_sale = temp }
        if let temp = json["stockx_deadstock_sold"] as? String { stockx_deadstock_sold = temp }
        if let temp = json["stockx_sales_last_72"] as? String { stockx_sales_last_72 = temp }
        if let temp = json["id"] as? String { id = temp }
        if let temp = json["created_at"] as? String { created_at = temp }
        if let temp = json["updated_at"] as? String { updated_at = temp }
        if let temp = json["product_id"] as? String { product_id = temp }
        if let temp = json["release_date_calendar"] as? String { release_date_calendar = temp }
        if let temp = json["release_date"] as? String { release_date = temp }
        if let temp = json["yes_votes"] as? String { yes_votes = temp }
        if let temp = json["no_votes"] as? String { no_votes = temp }
        if let temp = json["total_votes"] as? String { total_votes = temp }
        if let temp = json["yes_percentage"] as? String { yes_percentage = temp }
        if let temp = json["no_percentage"] as? String { no_percentage = temp }

        return self
    }

}
