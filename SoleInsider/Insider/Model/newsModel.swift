//
//  newsModel.swift
//  Insider
//
//  Created by admin on 15/07/2021.
//

import UIKit

class newsModel: NSObject {
    var id:String = ""
    var title:String = ""
    var description1:String=""
    var souce :String = ""
    var category :String = ""
    var slug:String = ""
    var hash1:String = ""
    var created_at :String = ""
    var updated_at :String = ""


    func initload(_ json: [String:Any]) -> newsModel {
        if let temp = json["id"] as? String { id = temp }
        if let temp = json["title"] as? String { title = temp }
        
        if let temp = json["description"] as? String { description1 = temp }
        
        if let temp = json["souce"] as? String { souce = temp }
        if let temp = json["category"] as? String { category = temp }
        if let temp = json["slug"] as? String { slug = temp }
        if let temp = json["hash1"] as? String { hash1 = temp }
        if let temp = json["created_at"] as? String { created_at = temp }
        if let temp = json["updated_at"] as? String { updated_at = temp }


        return self
    }

}
