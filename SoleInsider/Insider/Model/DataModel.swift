//
//  DataModel.swift
//  Insider
//
//  Created by admin on 29/07/2021.
//

import UIKit
import RealmSwift
class AddDicModel {
    var idpro:Int = 0
    var name:String = ""
    var release_date:String = ""
    var price:String = ""
    var stockx_thumbnail_url:String = ""
    var fav:Int = 0
    var direction:Int = 0
    var changed:Int = 0
    init(name:String,stockx_thumbnail_url:String, release_date:String, price:String, changed:Int, fav:Int, direction:Int, idpro: Int) {
        self.name = name
        self.stockx_thumbnail_url = stockx_thumbnail_url
        self.release_date = release_date
        self.price = price
        self.changed = changed
        self.fav = fav
        self.direction = direction
        self.idpro = idpro
    }
}
class DataShoes:Object {
    @objc dynamic var id:String = ""
    @objc dynamic var name:String = ""
    @objc dynamic var sku:String = ""
    @objc dynamic var image:String = ""
    @objc dynamic var link:String = ""
    @objc dynamic var colorway:String = ""
    @objc dynamic var description1:String = ""
    @objc dynamic var content:String = ""
    @objc dynamic var slug:String = ""
    @objc dynamic var price:String = ""
    @objc dynamic var coming_soon:String = ""
    @objc dynamic var views:String = ""
    @objc dynamic var resale:String = ""
    @objc dynamic var type:String = ""
    @objc dynamic var stockx_url:String = ""
    @objc dynamic var stockx_thumbnail_url:String = ""
    @objc dynamic var stockx_ticker_symbol:String = ""
    @objc dynamic var stockx_name:String = ""
    @objc dynamic var stockx_make:String = ""
    @objc dynamic var stockx_model:String = ""
    @objc dynamic var stockx_price:String = ""
    @objc dynamic var stockx_highest_bid:String = ""
    @objc dynamic var stockx_total_dollars:String = ""
    @objc dynamic var stockx_lowest_ask:String = ""
    @objc dynamic var stockx_last_sale:String = ""
    @objc dynamic var stockx_deadstock_sold:String = ""
    @objc dynamic var stockx_sales_last_72:String = ""
    @objc dynamic var created_at:String = ""
    @objc dynamic var updated_at:String = ""
    @objc dynamic var product_id:String = ""
    @objc dynamic var release_date_calendar:String = ""
    @objc dynamic var release_date:String = ""
    @objc dynamic var yes_votes:String = ""
    @objc dynamic var no_votes:String = ""
    @objc dynamic var total_votes:String = ""
    @objc dynamic var yes_percentage:String = ""
    @objc dynamic var no_percentage:String = ""
    @objc dynamic var fav:Int = 0
    @objc dynamic var idpro:Int = 0
    @objc dynamic var changed:Int = 0
    override class func primaryKey() -> String? {
        return "idpro"
    }
    func initload(_ sqlite:  [String: Any]) -> DataShoes {
        
        if let temp = sqlite["id"] as? String { id = temp }
        if let temp = sqlite["name"] as? String { name = temp }
        if let temp = sqlite["sku"] as? String { sku = temp }
        if let temp = sqlite["image"] as? String { image = temp }
        if let temp = sqlite["link"] as? String { link = temp }
        if let temp = sqlite["colorway"] as? String { colorway = temp }
        if let temp = sqlite["description"] as? String { description1 = temp }
        if let temp = sqlite["content"] as? String { content = temp }
        if let temp = sqlite["slug"] as? String { slug = temp }
        if let temp = sqlite["price"] as? String { price = temp }
        if let temp = sqlite["coming_soon"] as? String { coming_soon = temp }
        if let temp = sqlite["views"] as? String { views = temp }
        if let temp = sqlite["resale"] as? String { resale = temp }
        if let temp = sqlite["type"] as? String { type = temp }
        if let temp = sqlite["stockx_url"] as? String { stockx_url = temp }
        if let temp = sqlite["stockx_thumbnail_url"] as? String { stockx_thumbnail_url = temp }
        if let temp = sqlite["stockx_ticker_symbol"] as? String { stockx_ticker_symbol = temp }
        if let temp = sqlite["stockx_name"] as? String { stockx_name = temp }
        if let temp = sqlite["stockx_make"] as? String { stockx_make = temp }
        if let temp = sqlite["stockx_model"] as? String { stockx_model = temp }
        if let temp = sqlite["stockx_price"] as? String { stockx_price = temp }
        if let temp = sqlite["stockx_highest_bid"] as? String { stockx_highest_bid = temp }
        if let temp = sqlite["stockx_total_dollars"] as? String { stockx_total_dollars = temp }
        if let temp = sqlite["stockx_lowest_ask"] as? String { stockx_lowest_ask = temp }
        if let temp = sqlite["stockx_last_sale"] as? String { stockx_last_sale = temp }
        if let temp = sqlite["stockx_deadstock_sold"] as? String { stockx_deadstock_sold = temp }
        if let temp = sqlite["stockx_sales_last_72"] as? String { stockx_sales_last_72 = temp }
        if let temp = sqlite["id"] as? String { id = temp }
        if let temp = sqlite["created_at"] as? String { created_at = temp }
        if let temp = sqlite["updated_at"] as? String { updated_at = temp }
        if let temp = sqlite["product_id"] as? String { product_id = temp }
        if let temp = sqlite["release_date_calendar"] as? String { release_date_calendar = temp }
        if let temp = sqlite["release_date"] as? String { release_date = temp }
        if let temp = sqlite["yes_votes"] as? String { yes_votes = temp }
        if let temp = sqlite["no_votes"] as? String { no_votes = temp }
        if let temp = sqlite["total_votes"] as? String { total_votes = temp }
        if let temp = sqlite["yes_percentage"] as? String { yes_percentage = temp }
        if let temp = sqlite["no_percentage"] as? String { no_percentage = temp }
        if let temp = sqlite["fav"] as? Int { fav = temp }
        if let temp = sqlite["idpro"] as? Int { idpro = temp }
        if let temp = sqlite["changed"] as? Int { changed = temp }

        return self
}
    }
