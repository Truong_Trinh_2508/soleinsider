

import UIKit
class getCommentsModel_7209 : NSObject{
    var id:String = ""
    var member_id:String = ""
    var product_id:String = ""
    var comment:String = ""
    var votes_up:String = ""
    var votes_down:String = ""
    var created_at:String? = ""
    var updated_at:String? = ""
    var email:String? = ""
    var password:String? = ""
    var phone_number:String? = ""
    var carrier:String? = ""
    var member_type:String? = ""
    var verified:String? = ""
    var profile_image:String? = ""
    var bounced_email:String? = ""
    var comment_date:String = ""


        func initLoad(_ json:  [String: Any]) -> getCommentsModel_7209{
            if let temp = json["id"] as? String { id = temp }
            if let temp = json["member_id"] as? String { member_id = temp }
            if let temp = json["comment"] as? String { comment = temp }
            if let temp = json["votes_up"] as? String { votes_up = temp }
            if let temp = json["votes_down"] as? String { votes_down = temp }
            if let temp = json["product_id"] as? String { product_id = temp }
            if let temp = json["comment_date"] as? String { comment_date = temp }
            if let temp = json["created_at"] as? String? { created_at = temp }
            if let temp = json["updated_at"] as? String? { updated_at = temp }
            if let temp = json["email"] as? String? { email = temp }
            if let temp = json["password"] as? String? { password = temp }
            if let temp = json["phone_number"] as? String? { phone_number = temp }
            if let temp = json["carrier"] as? String? { carrier = temp }
            if let temp = json["member_type"] as? String? { member_type = temp }
            if let temp = json["verified"] as? String? { verified = temp }
            if let temp = json["profile_image"] as? String? { profile_image = temp }
            if let temp = json["bounced_email"] as? String? { bounced_email = temp }

            return self
}
}
