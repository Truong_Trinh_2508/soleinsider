//
//  getSlideShowModel.swift
//  Insider
//
//  Created by admin on 05/07/2021.
//

import UIKit
class getSlideShowModel: NSObject{
    var id:String = ""
    var product_id:String = ""
    var optimized:String = ""
    var image:String = ""
    
    func initload(_ json: [String:Any]) -> getSlideShowModel {
        if let temp = json["id"] as? String { id = temp }
        if let temp = json["product_id"] as? String { product_id = temp }
        if let temp = json["optimized"] as? String { optimized = temp }
        if let temp = json["image"] as? String { image = temp }

        return self
    }
}
